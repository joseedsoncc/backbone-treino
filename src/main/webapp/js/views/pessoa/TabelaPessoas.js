LinhaPessoa = Marionette.ItemView.extend({
	template : templateByUrl('js/views/pessoa/tpl/LinhaPessoa.html'),
	tagName : 'li',
	model : PessoaModel,
	events : {
		'click a' : 'jogaNaTela'
	},
	
	jogaNaTela : function(ev) {
		alert(JSON.stringify(this.model));
	},
});

TabelaPessoas = Marionette.CollectionView.extend({
	tagName : 'ul',
	itemView : LinhaPessoa,
})